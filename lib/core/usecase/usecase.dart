import 'package:equatable/equatable.dart';

abstract class UseCase<User>{
  Future<List<User>> call();
}