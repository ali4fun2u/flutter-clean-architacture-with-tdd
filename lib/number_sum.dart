import 'dart:developer';

int SumNumber(dynamic numA, dynamic numB){
    if (numA.runtimeType == String) {
      numA = int.parse(numA);
    }
    if (numB.runtimeType == String) {
      numB = int.parse(numB);
    }
    return numA + numB;
  }

void main() {
  try {
    SumNumber('abc', 1);
  } catch (FormatException) {
    log('One or both of the provided params is an invalid number as string');
  }
}