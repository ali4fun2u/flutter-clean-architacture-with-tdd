import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:newtddapp/core/usecase/usecase.dart';
import 'package:newtddapp/features/items_list/data/datasource/get_item_ds.dart';
import 'package:newtddapp/features/items_list/data/repository/get_items_repo.dart';
import 'package:newtddapp/features/items_list/domain/repository/get_items_repo.dart';
import 'package:newtddapp/features/items_list/domain/usecases/get_items_list.dart';
import 'package:newtddapp/features/items_list/presentation/pages/home.dart';

import 'features/items_list/presentation/mixin/get_items/get_items_list.dart';

final sl = GetIt.instance;

class Injector {}

Future<void> init() async {
  sl.registerLazySingleton(() => GetItemsList(sl()));

  sl.registerLazySingleton<IGetItemRepository>(
      () => GetItemRepository(iGetItemDataSource: sl()));

  sl.registerLazySingleton<IGetItemDataSource>(() => GetItemDataSource());

  sl.registerSingleton<ListItemsWatch>(
    ListItemsWatch());
}
