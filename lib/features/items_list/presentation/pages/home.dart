import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:newtddapp/features/items_list/presentation/widgets/itemsList.dart';

import '../../../../injector.dart';
import '../../domain/entities/item.dart';
import '../../domain/usecases/get_items_list.dart';
import '../mixin/get_items/get_items_list.dart';

class HomePage extends StatelessWidget with GetItMixin {

  late GetItemsList getItemsList;

  Future<void> loadData() async {
    getItemsList = GetItemsList(sl());
    get<ListItemsWatch>().setList(await getItemsList());
  }

  @override
  Widget build(BuildContext context) {

   final List<Item> items = watchOnly((ListItemsWatch lst) => lst.list);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: items.isEmpty
            ? const Text('No items')
            : ItemsList(listItems: items),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: loadData,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
