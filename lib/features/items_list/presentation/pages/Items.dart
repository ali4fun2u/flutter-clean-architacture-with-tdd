import 'package:flutter/material.dart';

import '../../domain/entities/item.dart';

class Items extends StatefulWidget {
  final List<Item> itemsList;
  const Items({Key? key, required this.itemsList}) : super(key: key);

  @override
  _ItemsState createState() => _ItemsState(this.itemsList);
}

class _ItemsState extends State<Items> {
  final List<Item> listItems;
  _ItemsState(this.listItems);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: listItems.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title:
              Text('${listItems[index].name} ${listItems[index].price} $index'),
        );
      },
    );
  }
}
