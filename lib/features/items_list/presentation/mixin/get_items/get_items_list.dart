import 'package:flutter/cupertino.dart';

import '../../../domain/entities/item.dart';

class ListItemsWatch extends ChangeNotifier {
  List<Item> _listItems = [];
  List<Item> get list => _listItems;

  void setList(List<Item> lst) {
    _listItems = lst;
    notifyListeners();
  }
}
