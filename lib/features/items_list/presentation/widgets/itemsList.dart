import 'package:flutter/material.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';

class ItemsList extends StatelessWidget {
  final List<Item> listItems;
  const ItemsList({Key? key, required this.listItems}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: listItems.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(
                '${listItems[index].name} ${listItems[index].price} $index'),
          );
        });
  }
}
