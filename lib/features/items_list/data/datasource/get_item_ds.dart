import 'package:newtddapp/features/items_list/data/models/item_model.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';

class GetItemDataSource implements IGetItemDataSource{

  late ItemModel itemModel;
  late List<ItemModel> itemsList;

  @override
  Future<List<ItemModel>> getItems() {
    ItemModel itemModel = ItemModel(name: 'New Item', description: 'item description', price: 30, image: 'url image');
    itemsList = [itemModel,itemModel,itemModel,itemModel,itemModel,itemModel,itemModel,itemModel,itemModel,itemModel];
   return  Future.value(itemsList);
  }

}


abstract class IGetItemDataSource {
  Future<List<ItemModel>> getItems();
}