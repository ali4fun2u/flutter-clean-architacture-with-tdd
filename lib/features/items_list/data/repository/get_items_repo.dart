import 'package:newtddapp/features/items_list/data/datasource/get_item_ds.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';
import 'package:newtddapp/features/items_list/domain/repository/get_items_repo.dart';

class GetItemRepository implements IGetItemRepository{

  final IGetItemDataSource iGetItemDataSource;

  GetItemRepository({required this.iGetItemDataSource});
  @override
  Future<List<Item>> getItems() async {
   return await iGetItemDataSource.getItems();
  }
}