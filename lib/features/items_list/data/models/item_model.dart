import 'package:newtddapp/features/items_list/domain/entities/item.dart';

class ItemModel extends Item{
  ItemModel({required super.name, required super.description, required super.price, required super.image});
}