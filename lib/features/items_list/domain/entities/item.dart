import 'package:equatable/equatable.dart';

class Item  extends Equatable{
  final String name;
  final String description;
  final double price;
  final String image;

  Item({required this.name,required this.description, required this.price, required this.image});
  
  @override
  List<Object?> get props => [name,description,price,image];
}