import 'package:newtddapp/features/items_list/domain/entities/item.dart';

abstract class IGetItemRepository {
  Future<List<Item>> getItems();
}