import 'package:newtddapp/core/usecase/usecase.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';
import 'package:newtddapp/features/items_list/domain/repository/get_items_repo.dart';

class GetItemsList extends UseCase<Item>{
  final IGetItemRepository getItemRepository;

  GetItemsList(this.getItemRepository);

  @override
  Future<List<Item>> call() async {
    return await getItemRepository.getItems();
  }
}