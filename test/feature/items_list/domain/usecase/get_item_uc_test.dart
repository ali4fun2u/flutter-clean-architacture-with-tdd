import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';
import 'package:newtddapp/features/items_list/domain/repository/get_items_repo.dart';
import 'package:newtddapp/features/items_list/domain/usecases/get_items_list.dart';

import 'get_item_uc_test.mocks.dart';



@GenerateMocks([IGetItemRepository])
void main() {

  GetItemsList? getItemsList;
  MockIGetItemRepository? mockIGetItemRepository;
  
  setUp((){
    mockIGetItemRepository = MockIGetItemRepository();
    getItemsList = GetItemsList(mockIGetItemRepository!);
  });
  group('get items list', (){

    Item item = Item(name: 'New Item', description: 'item description', price: 30, image: 'url image');
    List<Item> itemsList = [item,item,item,item,item,item,item,item,item,item];

    test('should return items list', () async{

      when(mockIGetItemRepository?.getItem()).thenAnswer((_) async => itemsList);

      final result = await getItemsList!();

      expect(result, itemsList);
    });


     test('should return 10 items in list', () async{

      when(mockIGetItemRepository?.getItem()).thenAnswer((_) async => itemsList);

      final result = await getItemsList!();
      final itemLength = result.length;

      expect(itemLength, 10);
    });
  });
}