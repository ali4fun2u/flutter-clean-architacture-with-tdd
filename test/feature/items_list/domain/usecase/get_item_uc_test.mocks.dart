/// Mocks generated by Mockito 5.2.0 from annotations
// in newtddapp/test/feature/domain/usecase/get_item_uc_test.dart.
// Do not manually edit this file.

import 'dart:async' as _i3;

import 'package:mockito/mockito.dart' as _i1;
import 'package:newtddapp/features/items_list/domain/entities/item.dart' as _i4;
import 'package:newtddapp/features/items_list/domain/repository/get_items_repo.dart'
    as _i2;

// ignore_for_file: type=lint
// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis
// ignore_for_file: camel_case_types

/// A class which mocks [IGetItemRepository].
///
/// See the documentation for Mockito's code generation for more information.
class MockIGetItemRepository extends _i1.Mock
    implements _i2.IGetItemRepository {
  MockIGetItemRepository() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i3.Future<List<_i4.Item>> getItem() =>
      (super.noSuchMethod(Invocation.method(#getItem, []),
              returnValue: Future<List<_i4.Item>>.value(<_i4.Item>[]))
          as _i3.Future<List<_i4.Item>>);
}
