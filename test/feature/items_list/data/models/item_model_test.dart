import 'dart:ffi';

import 'package:flutter_test/flutter_test.dart';
import 'package:newtddapp/features/items_list/data/models/item_model.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';

void main() {
  group('Item model verify', (){
    final itemModel = ItemModel(name: 'name', description: 'description', price: 22, image: 'image');
    test('Item model should be accept item', (){

      expect(itemModel, isA<Item>());

    });

    test('should be correct type of attribute.', (){
      Type type;

      type = itemModel.name.runtimeType;
      expect(type, String);

      type = itemModel.description.runtimeType;
      expect(type, String);

      type = itemModel.price.runtimeType;
      expect(type, double);

      type = itemModel.image.runtimeType;
      expect(type, String);

    });
  });
}