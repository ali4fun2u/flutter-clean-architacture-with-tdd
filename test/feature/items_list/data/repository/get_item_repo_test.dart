import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:newtddapp/features/items_list/data/datasource/get_item_ds.dart';
import 'package:newtddapp/features/items_list/data/models/item_model.dart';
import 'package:newtddapp/features/items_list/data/repository/get_items_repo.dart';

import '../datasource/get_item_ds_test.mocks.dart';

@GenerateMocks([IGetItemDataSource])
void main() {

  GetItemRepository? getItemRepository;
   MockIGetItemDataSource? mockIGetItemDataSource;

  setUp((){
    mockIGetItemDataSource = MockIGetItemDataSource();
    getItemRepository = GetItemRepository(iGetItemDataSource: mockIGetItemDataSource!);
  });
  group('should get list of items.', (){

    ItemModel item = ItemModel(name: 'New Item', description: 'item description', price: 30, image: 'url image');
    List<ItemModel> itemsList = [item,item,item,item,item,item,item,item,item,item];


    test('should return list of items.', () async{
        when(mockIGetItemDataSource?.getItems()).thenAnswer((_)async => itemsList );

        final result = await getItemRepository?.getItems();

        expect(result, itemsList);
    });

    test('should return list of 10 items.', () async{
        when(mockIGetItemDataSource?.getItems()).thenAnswer((_)async => itemsList );

        final result = await getItemRepository?.getItems();
         final itemsLength = result?.length;

        expect(itemsLength, itemsList.length);
    });

  });
}