import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:newtddapp/features/items_list/data/datasource/get_item_ds.dart';
import 'package:newtddapp/features/items_list/data/models/item_model.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';

import 'get_item_ds_test.mocks.dart';

@GenerateMocks([IGetItemDataSource])
void main() {

  GetItemDataSource? getItemDataSource;
  MockIGetItemDataSource? mockIGetItemDataSource;
  setUp( (){
    getItemDataSource = GetItemDataSource();
    mockIGetItemDataSource = MockIGetItemDataSource();
  });
  group('Get list of 10 item.', (){
    ItemModel item = ItemModel(name: 'New Item', description: 'item description', price: 30, image: 'url image');
    List<ItemModel> itemsList = [item,item,item,item,item,item,item,item,item,item];

    test('should return a list of item.', () async{
      when(mockIGetItemDataSource?.getItems()).thenAnswer((_) async => itemsList);

      final result = await  getItemDataSource?.getItems();

      expect(result, itemsList);

    });

    test('should return 10 items.', () async{

      final result = await  getItemDataSource?.getItems();
      final itemLength = result?.length;

      expect(itemLength, 10);
    });
  });
}