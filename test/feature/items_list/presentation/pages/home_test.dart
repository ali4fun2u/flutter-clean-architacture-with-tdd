import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:newtddapp/features/items_list/data/datasource/get_item_ds.dart';
import 'package:newtddapp/features/items_list/data/repository/get_items_repo.dart';
import 'package:newtddapp/features/items_list/domain/repository/get_items_repo.dart';
import 'package:newtddapp/features/items_list/domain/usecases/get_items_list.dart';
import 'package:newtddapp/features/items_list/presentation/mixin/get_items/get_items_list.dart';
import 'package:newtddapp/features/items_list/presentation/pages/home.dart';
import 'package:newtddapp/features/items_list/presentation/widgets/itemsList.dart';

import '../../domain/usecase/get_item_uc_test.mocks.dart';

final sl = GetIt.instance;

void main() {
  sl.registerLazySingleton(() => GetItemsList(sl()));

  sl.registerLazySingleton<IGetItemRepository>(
      () => GetItemRepository(iGetItemDataSource: sl()));

  sl.registerLazySingleton<IGetItemDataSource>(() => GetItemDataSource());

  sl.registerSingleton<ListItemsWatch>(ListItemsWatch());

  group('Test home widget', () {
    final testWidget = MaterialApp(
      home: HomePage(),
    );

    GetIt.I.registerLazySingleton<MockIGetItemRepository>(
        () => MockIGetItemRepository());

    testWidgets('home widget created', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);

      expect(find.byType(FloatingActionButton), findsOneWidget);
      expect(find.text('Home'), findsOneWidget);
    });

    testWidgets('show no item when item not loaded....',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);

      expect(find.text('No items'), findsOneWidget);
    });
    testWidgets('floating button tap show list.', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);

      await tester.tap(find.byType(FloatingActionButton));
      await tester.pumpAndSettle();
      expect(find.text('No items'), findsNothing);
    });

    testWidgets('on button press should return data from data source.',
        (WidgetTester) async {
      GetIt.I.registerSingleton(IGetItemRepository);
    });

    testWidgets('show Items list widget ', ((tester) async {
      await tester.pumpWidget(testWidget);

      await tester.tap(find.byType(FloatingActionButton));
      await tester.pump();

      expect(find.byType(ItemsList), findsOneWidget);
      expect(find.text('No items'), findsNothing);
    }));
  });
}
// refrance https://stackoverflow.com/questions/70098708/unit-tests-running-in-parallel-in-flutter-when-using-getit