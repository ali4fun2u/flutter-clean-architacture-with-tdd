import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:newtddapp/features/items_list/domain/entities/item.dart';
import 'package:newtddapp/features/items_list/presentation/widgets/itemsList.dart';

void main() {
  group('should show a list of items.', (() {
    testWidgets('show list of items.', ((widgetTester) async {
      Item item = Item(
          name: 'name', description: 'description', price: 33, image: 'image');
      List<Item> list = [item, item, item, item, item];

      await widgetTester.pumpWidget(MaterialApp(
          home: Material(child: Center(child: ItemsList(listItems: list)))));

      await widgetTester.pump();

      expect(find.textContaining('name'), findsNWidgets(5));
        
    }));
  }));
}
