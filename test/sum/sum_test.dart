import 'package:flutter_test/flutter_test.dart';
import 'package:newtddapp/number_sum.dart';

void main() {
  int a = 0;
  int b = 4;

  group('Test function sum', () {
    test('should return sum of integer number', () {
      int sumNumber = SumNumber(a, b);
      final result = a + b;
      expect(sumNumber, result);
    });

    test('should return sum of both value even first value string', () {
      const String a = '2';
      const int b = 3;
      int sumNumber = SumNumber(a, b);
      final result = int.parse(a) + b;
      expect(sumNumber, result);
    });

    test('should return sum of both value even second value string', () {
      const int a = 2;
      const String b = '4';
      int sumNumber = SumNumber(a, b);
      final result = int.parse(b) + a;
      expect(sumNumber, result);
    });

    test('should return sum of both value even if both values are string', () {
      const String a = '2';
      const String b = '4';
      int sumNumber = SumNumber(a, b);
      final result = int.parse(b) + int.parse(a);
      expect(sumNumber, result);
    });

    test('Should fail if provided value for numA is invalid number as string', () {
      const String a = 'abc';
      const String b = '4';
      expect(() => SumNumber(a, b), throwsA(TypeMatcher<FormatException>()));
    });
  });
}
